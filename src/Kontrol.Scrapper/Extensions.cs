﻿using System;
using Newtonsoft.Json.Linq;

namespace Kontrol.Scrapper
{
    public static class Extensions
    {
         public static T Get<T>(this JToken token, string property)
         {
             if(string.IsNullOrWhiteSpace(property)) throw new ArgumentNullException("property");

             if (token == null) return default(T);
             var propertyToken = token.SelectToken(property);
             if (propertyToken == null) return default(T);
             try
             {
                 return propertyToken.Value<T>();
             }
             catch (Exception)
             {
                 return default(T);
             }
         }
    }
}