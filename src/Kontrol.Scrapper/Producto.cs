﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Kontrol.Scrapper
{
    [Table("Productos")]
    public class Producto : Entity, IEquatable<Producto>
    {
        public bool Activo { get; set; }
        public string CodigoDeBarra { get; set; }
        public long CategoriaId { get; set; }
        public string Descripcion { get; set; }
        public DateTime FechaAlta { get; set; }
        public DateTime? FechaBaja { get; set; }
        public long IdExterno { get; set; }
        public long ImagenChicaId { get; set; }
        public long ImagenGrandeId { get; set; }

        public string Marca { get; set; }
        public string Nombre { get; set; }
        public string Observaciones { get; set; }
        public string Peso { get; set; }
        public string TipoEnvase { get; set; }

        public byte[] ImagenChica { get; set; }
        public byte[] ImagenGrande { get; set; }

        public DateTime FechaActualizacion { get; set; }

        public bool Equals(Producto other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Activo.Equals(other.Activo) && string.Equals(CodigoDeBarra, other.CodigoDeBarra) && CategoriaId == other.CategoriaId && string.Equals(Descripcion, other.Descripcion) && FechaAlta.Equals(other.FechaAlta) && FechaBaja.Equals(other.FechaBaja) && IdExterno == other.IdExterno && ImagenChicaId == other.ImagenChicaId && ImagenGrandeId == other.ImagenGrandeId && string.Equals(Marca, other.Marca) && string.Equals(Nombre, other.Nombre) && string.Equals(Observaciones, other.Observaciones) && string.Equals(Peso, other.Peso) && string.Equals(TipoEnvase, other.TipoEnvase) && Equals(ImagenChica, other.ImagenChica) && Equals(ImagenGrande, other.ImagenGrande);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Producto)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = Activo.GetHashCode();
                hashCode = (hashCode * 397) ^ (CodigoDeBarra != null ? CodigoDeBarra.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ CategoriaId.GetHashCode();
                hashCode = (hashCode * 397) ^ (Descripcion != null ? Descripcion.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ FechaAlta.GetHashCode();
                hashCode = (hashCode * 397) ^ FechaBaja.GetHashCode();
                hashCode = (hashCode * 397) ^ IdExterno.GetHashCode();
                hashCode = (hashCode * 397) ^ ImagenChicaId.GetHashCode();
                hashCode = (hashCode * 397) ^ ImagenGrandeId.GetHashCode();
                hashCode = (hashCode * 397) ^ (Marca != null ? Marca.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Nombre != null ? Nombre.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Observaciones != null ? Observaciones.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Peso != null ? Peso.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (TipoEnvase != null ? TipoEnvase.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (ImagenChica != null ? ImagenChica.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (ImagenGrande != null ? ImagenGrande.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static bool operator ==(Producto left, Producto right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Producto left, Producto right)
        {
            return !Equals(left, right);
        }
    }
}