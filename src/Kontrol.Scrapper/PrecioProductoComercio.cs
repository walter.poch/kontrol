﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Kontrol.Scrapper
{
    [Table("Precios")]
    public class PrecioProductoComercio : Entity, IEquatable<PrecioProductoComercio>
    {
        public Comercio Comercio { get; set; }
        public Producto Producto { get; set; }

        public Guid ComercioId { get; set; }
        public Guid ProductoId { get; set; }

        public decimal Precio { get; set; }
        public DateTime Fecha { get; set; }

        public bool Equals(PrecioProductoComercio other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(Comercio, other.Comercio) && Equals(Producto, other.Producto) && Precio == other.Precio && Fecha.Equals(other.Fecha);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((PrecioProductoComercio)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = (Comercio != null ? Comercio.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Producto != null ? Producto.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Precio.GetHashCode();
                hashCode = (hashCode * 397) ^ Fecha.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(PrecioProductoComercio left, PrecioProductoComercio right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(PrecioProductoComercio left, PrecioProductoComercio right)
        {
            return !Equals(left, right);
        }
    }
}