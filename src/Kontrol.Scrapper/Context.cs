﻿using System;
using System.Data.Entity;
using System.Data.SqlTypes;

namespace Kontrol.Scrapper
{
    public class Context : DbContext
    {
        public Context() : base("Context") { }

        public DbSet<Comercio> Comercios { get; set; }
        public DbSet<Producto> Productos { get; set; }
        public DbSet<PrecioProductoComercio> Precios { get; set; }

        public override int SaveChanges()
        {
            UpdateDates();
            return base.SaveChanges();
        }

        private void UpdateDates()
        {
            foreach (var change in ChangeTracker.Entries<Entity>())
            {
                var values = change.CurrentValues;
                foreach (var name in values.PropertyNames)
                {
                    var value = values[name];
                    if (value is DateTime)
                    {
                        var date = (DateTime)value;
                        if (date < SqlDateTime.MinValue.Value)
                        {
                            values[name] = SqlDateTime.MinValue.Value;
                        }
                        else if (date > SqlDateTime.MaxValue.Value)
                        {
                            values[name] = SqlDateTime.MaxValue.Value;
                        }
                    }
                }
            }
        }
    }
}