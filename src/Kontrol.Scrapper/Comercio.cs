﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Kontrol.Scrapper
{
    [Table("Comercios")]
    public class Comercio : Entity, IEquatable<Comercio>
    {
        public bool Activo { get; set; }
        public long CadenaId { get; set; }
        public string Calle { get; set; }
        public string CiudadOBarrio { get; set; }
        public string CodigoPostal { get; set; }
        public string CodigoSucursal { get; set; }
        public string Contactos { get; set; }
        public long Cuit { get; set; }
        public string Fax { get; set; }
        public long IdExterno { get; set; }
        public double Latitud { get; set; }
        public long LocalidadId { get; set; }
        public double Longitud { get; set; }
        public string NombreFantasia { get; set; }
        public long Numero { get; set; }
        public string Observaciones { get; set; }
        public string Punto { get; set; }
        public string RazonSocial { get; set; }
        public string Subcadena { get; set; }
        public string Telefono { get; set; }
        public string UrlPersonalizada { get; set; }
        public string Provincia { get; set; }

        public DateTime FechaActualizacion { get; set; }

        public bool Equals(Comercio other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Activo.Equals(other.Activo) && CadenaId == other.CadenaId && string.Equals(Calle, other.Calle) && string.Equals(CiudadOBarrio, other.CiudadOBarrio) && string.Equals(CodigoPostal, other.CodigoPostal) && string.Equals(CodigoSucursal, other.CodigoSucursal) && string.Equals(Contactos, other.Contactos) && Cuit == other.Cuit && string.Equals(Fax, other.Fax) && IdExterno == other.IdExterno && Latitud.Equals(other.Latitud) && LocalidadId == other.LocalidadId && Longitud.Equals(other.Longitud) && string.Equals(NombreFantasia, other.NombreFantasia) && Numero == other.Numero && string.Equals(Observaciones, other.Observaciones) && string.Equals(Punto, other.Punto) && string.Equals(RazonSocial, other.RazonSocial) && string.Equals(Subcadena, other.Subcadena) && string.Equals(Telefono, other.Telefono) && string.Equals(UrlPersonalizada, other.UrlPersonalizada) && string.Equals(Provincia, other.Provincia);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Comercio)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = Activo.GetHashCode();
                hashCode = (hashCode * 397) ^ CadenaId.GetHashCode();
                hashCode = (hashCode * 397) ^ (Calle != null ? Calle.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (CiudadOBarrio != null ? CiudadOBarrio.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (CodigoPostal != null ? CodigoPostal.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (CodigoSucursal != null ? CodigoSucursal.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Contactos != null ? Contactos.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Cuit.GetHashCode();
                hashCode = (hashCode * 397) ^ (Fax != null ? Fax.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ IdExterno.GetHashCode();
                hashCode = (hashCode * 397) ^ Latitud.GetHashCode();
                hashCode = (hashCode * 397) ^ LocalidadId.GetHashCode();
                hashCode = (hashCode * 397) ^ Longitud.GetHashCode();
                hashCode = (hashCode * 397) ^ (NombreFantasia != null ? NombreFantasia.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Numero.GetHashCode();
                hashCode = (hashCode * 397) ^ (Observaciones != null ? Observaciones.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Punto != null ? Punto.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (RazonSocial != null ? RazonSocial.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Subcadena != null ? Subcadena.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Telefono != null ? Telefono.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (UrlPersonalizada != null ? UrlPersonalizada.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Provincia != null ? Provincia.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static bool operator ==(Comercio left, Comercio right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Comercio left, Comercio right)
        {
            return !Equals(left, right);
        }    
    }
}