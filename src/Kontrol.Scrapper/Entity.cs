﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Kontrol.Scrapper
{
    public abstract class Entity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public virtual Guid Id { get; set; }         
    }
}