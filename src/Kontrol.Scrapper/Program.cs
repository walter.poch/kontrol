﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using HtmlAgilityPack;
using Newtonsoft.Json.Linq;
using Noesis.Javascript;
using log4net;

namespace Kontrol.Scrapper
{
    class Program
    {
        private static ILog Log;

        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();
            Log = LogManager.GetLogger(typeof(Program));

            Console.WriteLine("Iniciando app...");
            try
            {
                Database.SetInitializer(new DropCreateDatabaseIfModelChanges<Context>());

                ObtenerProductosAuditados();
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR: {0}", ex);
            }
            Console.WriteLine("FIN!!!");
            Console.ReadKey();
        }

        private static void ObtenerProductosAuditados()
        {
            const string URL = "http://www.preciosensusitio.gob.ar/busquedas/seleccionar_articulos";
            const string URL_PRODUCTO =
                "http://www.preciosensusitio.gob.ar/comercio_articulos/traer_descripcion_articulo/1282";
            const string URL_JSON_PRODUCTO =
            "http://www.preciosensusitio.gob.ar/busquedas/asyncObtenerSupermercadosPreciosEnRadio?latitud=-34.6036381&longitud=-58.38160370000003&radio=999999999999999999999999999999999999999999999999999&idArticulo={0}";
            const string URL_IMAGEN = "http://www.preciosensusitio.gob.ar/comercio_articulos/get_imagen/{0}";

            var client = new HtmlWeb();
            var doc = client.Load(URL);
            var script = doc.DocumentNode.SelectSingleNode("//*[@id='content']/script").InnerText;

            //Dejo solo para parte que arma el arreglo con los nombres
            var inicio = script.IndexOf("var nombres = [];");
            var fin = script.IndexOf("nombres.sort(function(a,b){");
            script = "var articulos = {};\n" + script.Substring(inicio, fin - inicio);

            List<long> productosId;
            using (var context = new JavascriptContext())
            {
                context.Run(script);
                var articulos = (Dictionary<string, object>)context.GetParameter("articulos");
                productosId = articulos.Select(p => Convert.ToInt64(p.Value)).ToList();
            }

            productosId.AsParallel().ForAll(id =>
            {
                Log.InfoFormat("Procesando producto: {0}", id);

                var wc = new WebClient();
                try
                {
                    var url = string.Format(URL_JSON_PRODUCTO, id);
                    var json = wc.DownloadString(url);
                    var obj = JArray.Parse(json);

                    var producto = new Producto();
                    var comercio = new Comercio();

                    foreach (var item in obj)
                    {
                        // Producto
                        var productoJson = item.SelectToken("ComercioArticulo");

                        if (productoJson != null)
                        {
                            using (var ctx = new Context())
                            {
                                var productoNuevo = false;
                                var productoIdExt = productoJson.Get<long>("id");
                                var p = ctx.Productos.FirstOrDefault(x => x.IdExterno == productoIdExt);
                                if (p == null)
                                {
                                    p = new Producto { Id = Guid.NewGuid() };
                                    productoNuevo = true;
                                }

                                p.Activo = productoJson.Get<bool>("activo");
                                p.CodigoDeBarra = productoJson.Get<string>("codigo_de_barras");
                                p.CategoriaId = productoJson.Get<long>("comercio_articulo_categoria_id");
                                p.Descripcion = productoJson.Get<string>("descripcion");
                                p.FechaAlta = productoJson.Get<DateTime>("fecha_alta");
                                p.FechaBaja = productoJson.Get<DateTime?>("fecha_baja");
                                p.IdExterno = productoIdExt;
                                p.ImagenChicaId = productoJson.Get<long>("imagen_chica_id");
                                p.ImagenGrandeId = productoJson.Get<long>("imagen_grande_id");
                                p.Marca = productoJson.Get<string>("marca");
                                p.Nombre = productoJson.Get<string>("nombre");
                                p.Observaciones = productoJson.Get<string>("observaciones");
                                p.Peso = productoJson.Get<string>("peso");
                                p.TipoEnvase = productoJson.Get<string>("tipo_de_envase");

                                //Bajo las imágenes
                                producto.ImagenChica = wc.DownloadData(string.Format(URL_IMAGEN, producto.ImagenChicaId));
                                producto.ImagenGrande = wc.DownloadData(string.Format(URL_IMAGEN, producto.ImagenGrandeId));

                                if (producto != p)
                                {
                                    producto = p;
                                    producto.FechaActualizacion = DateTime.Now;
                                    if (productoNuevo)
                                    {
                                        ctx.Productos.Add(producto);
                                        Log.InfoFormat("Insertando el producto: {0}", producto.Id);
                                    }
                                }

                                ctx.SaveChanges();
                            }
                        }

                        //Comercio
                        var comercioJson = item.SelectToken("Comercio");
                        if (comercioJson != null)
                        {
                            using (var ctx = new Context())
                            {
                                var comercioNuevo = false;
                                var comercioIdExt = comercioJson.Get<long>("id");
                                var c = ctx.Comercios.FirstOrDefault(x => x.IdExterno == comercioIdExt);
                                if (c == null)
                                {
                                    c = new Comercio { Id = Guid.NewGuid() };
                                    comercioNuevo = true;
                                }

                                c.Activo = comercioJson.Get<bool>("activo");
                                c.CadenaId = comercioJson.Get<long>("cadena_id");
                                c.Calle = comercioJson.Get<string>("calle");
                                c.CiudadOBarrio = comercioJson.Get<string>("ciudad_o_barrio");
                                c.CodigoPostal = comercioJson.Get<string>("codigo_postal");
                                c.CodigoSucursal = comercioJson.Get<string>("codigo_sucursal");
                                c.Contactos = comercioJson.Get<string>("contactos");
                                c.Cuit = comercioJson.Get<long>("cuit");
                                c.Fax = comercioJson.Get<string>("fax");
                                c.IdExterno = comercioIdExt;
                                c.Latitud = comercioJson.Get<double>("latitud");
                                c.LocalidadId = comercioJson.Get<long>("localidad_id");
                                c.Longitud = comercioJson.Get<double>("longitud");
                                c.NombreFantasia = comercioJson.Get<string>("nombre_fantasia");
                                c.Numero = comercioJson.Get<long>("numero");
                                c.Observaciones = comercioJson.Get<string>("observaciones");
                                c.Punto = comercioJson.Get<string>("punto");
                                c.RazonSocial = comercioJson.Get<string>("razon_social");
                                c.Subcadena = comercioJson.Get<string>("subcadena");
                                c.Telefono = comercioJson.Get<string>("telefono");
                                c.UrlPersonalizada = comercioJson.Get<string>("url_personalizada");
                                c.Provincia = comercioJson.Get<string>("xprovincia");

                                if (comercio != c)
                                {
                                    comercio = c;
                                    comercio.FechaActualizacion = DateTime.Now;

                                    if (comercioNuevo)
                                    {
                                        ctx.Comercios.Add(comercio);
                                        Log.InfoFormat("Insertando el comercio: {0}", comercio.Id);
                                    }
                                }

                                ctx.SaveChanges();
                            }
                        }

                        //Precio
                        var precioJson = item.SelectToken("ComercioPrecio");
                        if (precioJson != null)
                        {
                            using (var ctx = new Context())
                            {
                                var precio = precioJson.Get<decimal>("precio");
                                var ppc = ctx.Precios.FirstOrDefault(x =>
                                    x.ComercioId == comercio.Id
                                    && x.ProductoId == producto.Id
                                    && x.Precio == precio);

                                if (ppc == null)
                                {
                                    ppc = new PrecioProductoComercio { Id = Guid.NewGuid() };

                                    ppc.ComercioId = comercio.Id;
                                    ppc.ProductoId = producto.Id;
                                    ppc.Precio = precio;
                                    ppc.Fecha = DateTime.Now;

                                    ctx.Precios.Add(ppc);
                                    Log.InfoFormat("Insertando el precio: {0} para el producto: {1} en el comercio: {2}", precio, producto.Nombre, comercio.RazonSocial);
                                }

                                ctx.SaveChanges();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.ErrorFormat("ERROR: ProductoId:{0} - Exc. {1}", id, ex);
                }
            });

            // Desactivo los productos que no estén más disponibles de selección
            using(var ctx = new Context())
            {
                ctx.Productos.Where(p => !productosId.Contains(p.IdExterno)).ToList().ForEach(p=>
                    {
                        p.Activo = false;
                        p.FechaActualizacion = DateTime.Now;
                    } );

                ctx.SaveChanges();
            }
        }
    }
}
